﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb.Models;

namespace MyWeb.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string username, string password, string name, string lastname, string tel)
        {
            
                using (UsersEntities db = new UsersEntities())
                {
                ACCOUNT a = new ACCOUNT()
                {
                    UID = Guid.NewGuid(),
                    USERNAME = username,
                    PASSWORD = password,
                    NAME = name,
                    LASTNAME = lastname,
                    TEL = tel
                };
                db.ACCOUNTs.Add(a);
                db.SaveChanges();
                }

                
            return View();

        }
    }
}
